# StarCraft 2 Counters

A page with StarCraft 2 Counters.

To scrape infos and generate the page run:

    ./render.py

You can use something like entr to rebuild on change:

    find *.py tpl.html | entr ./render.py