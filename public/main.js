
function sortRaces () {
    let player = document.getElementById('player-dropdown').value;
    let opponent = document.getElementById('opponent-dropdown').value;
    let protossContainer = document.getElementsByClassName('unit-container protoss');
    let terranContainer = document.getElementsByClassName('unit-container humans');
    let zergContainer = document.getElementsByClassName('unit-container zergs');
    let protossWeak = document.getElementsByClassName('weak protoss');
    let terranWeak = document.getElementsByClassName('weak humans');
    let zergWeak = document.getElementsByClassName('weak zergs');
    let protossStrong = document.getElementsByClassName('strong protoss');
    let terranStrong = document.getElementsByClassName('strong humans');
    let zergStrong = document.getElementsByClassName('strong zergs');

    let allLists = [protossContainer, zergContainer, terranContainer, protossStrong, zergStrong,
        terranStrong, protossWeak, terranWeak, zergWeak];
    let containerLists = [protossContainer, zergContainer, terranContainer];
    let strongLists = [protossStrong, terranStrong, zergStrong];
    let weakLists = [protossWeak, terranWeak, zergWeak];

    // hide all lists once at the start, display only the ones you want
    for (let i = 0; i < allLists.length; i++) {
        hideRace(allLists[i])
    }

    // display the strong options for the race you play
    if (player === "All") {
        for (let i = 0; i < 3; i++) {
        displayRace(strongLists[i]);
        displayRace(weakLists[i]);
    }
    }
    else if (player === "Protoss") {
        displayRace(protossWeak);
    }
    else if (player === "Terran") {
        displayRace(terranWeak);
    }
    else if (player === "Zerg") {
        displayRace(zergWeak);
    }

    // display the container for the race your opponent plays
    if (opponent === "All") {
        displayRace(protossContainer);
        displayRace(terranContainer);
        displayRace(zergContainer);
    }
    else if (opponent === "Protoss") {
        displayRace(protossContainer);
        hideRace(zergContainer);
        hideRace(terranContainer);
    }
    else if (opponent === "Terran") {
        displayRace(terranContainer);
        hideRace(zergContainer);
        hideRace(protossContainer);
    }
    else if (opponent === "Zerg") {
        displayRace(zergContainer);
        hideRace(protossContainer);
        hideRace(terranContainer);
    }


function displayRace(race) {
    for (let j = 0; j < race.length; j++) {
        race[j].setAttribute("style", "display: inline");
    }
}

function hideRace(race) {
    for (let j = 0; j < race.length; j++) {
        race[j].setAttribute("style", "display: none");
    }
}



}
function gridChange() {
    let grid = document.getElementById("grid-table");
    let value = document.getElementById('grid-dropdown').value;
    grid.style.setProperty('grid-template-columns', `repeat(${value}, 1fr)`);
}
